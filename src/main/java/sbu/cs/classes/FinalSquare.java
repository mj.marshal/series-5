package sbu.cs.classes;


import sbu.cs.data.Direction;
import sbu.cs.interfaces.Square;

public class FinalSquare implements Square {
    private String result = null;

    @Override
    public void setOutputSquare(Square neighbour, Direction direction) {
    }

    @Override
    public void setInputString(String input, Direction direction) {
        this.result = input;
    }

    @Override
    public boolean isReady() {
        return result != null;
    }

    @Override
    public String calculate() {
        if (isReady()) {
         return result;
        }
        return null;
    }
}
