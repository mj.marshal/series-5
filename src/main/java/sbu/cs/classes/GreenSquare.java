package sbu.cs.classes;


import sbu.cs.data.Direction;
import sbu.cs.interfaces.BlackFunction;
import sbu.cs.interfaces.Square;

public class GreenSquare implements Square {
    private final BlackFunction function;
    private Square horizontalOutput;
    private Square verticalOutput;

    private String input = null;

    public GreenSquare(BlackFunction function) {
        this.function = function;
    }


    @Override
    public void setOutputSquare(Square output, Direction direction) {
        switch (direction) {
            case VERTICAL:
                verticalOutput = output;
                break;
            case HORIZONTAL:
                horizontalOutput = output;
                break;
        }
    }

    @Override
    public void setInputString(String input, Direction direction) {
        this.input = input;
    }

    @Override
    public boolean isReady() {
        return input != null;
    }

    @Override
    public String calculate() {
        if (!isReady()) {
            return null;
        }

        String result = function.func(input);

        horizontalOutput.setInputString(result, Direction.HORIZONTAL);
        verticalOutput.setInputString(result, Direction.VERTICAL);
        String first = horizontalOutput.calculate();
        String second = verticalOutput.calculate();
        if (first != null) return first;
        return second;
    }
}
