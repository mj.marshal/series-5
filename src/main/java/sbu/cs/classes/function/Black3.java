package sbu.cs.classes.function;


import sbu.cs.interfaces.BlackFunction;

public class Black3 implements BlackFunction {
    @Override
    public String func(String arg) {
        StringBuilder ans = new StringBuilder(arg);
        ans.append(arg);
        return ans.toString();
    }
}
