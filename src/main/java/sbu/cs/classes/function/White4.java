package sbu.cs.classes.function;


import sbu.cs.interfaces.WhiteFunction;

public class White4 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        if( arg1.length() % 2 == 0 ){
            return arg1;
        }
        else {
            return arg2;
        }
    }
}
