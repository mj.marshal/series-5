package sbu.cs.classes.function;


import sbu.cs.interfaces.WhiteFunction;

public class White3 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        String lng;
        String shrt;
        int l;
        StringBuilder ans = new StringBuilder();
        if( arg1.length() >= arg2.length() ){
            lng = arg1;
            shrt = arg2;
            l = 1;
        }
        else {
            lng = arg2;
            shrt = arg1;
            l = 2;
        }
        for( int i = 0; i < shrt.length(); i ++ ){
            ans.append(arg1.charAt(i));
            ans.append(arg2.charAt( arg2.length() - 1 - i));
        }
        if( arg1.length() == arg2.length() ){
            return ans.toString();
        }
        else if( arg1.length() > arg2.length() ){
            for( int i = arg2.length(); i < arg1.length(); i ++ ){
                ans.append(arg1.charAt(i));
            }
        }
        else{
            for( int i = arg2.length() - 1; i >= arg1.length(); i -- ){
                ans.append(arg2.charAt(i));
            }
        }
        return ans.toString();
    }
}
