package sbu.cs.classes.function;


import sbu.cs.interfaces.BlackFunction;

public class Black1 implements BlackFunction {

    @Override
    public String func(String arg) {
        StringBuilder ans = new StringBuilder(arg);
        ans.reverse();
        return ans.toString();
    }
}
