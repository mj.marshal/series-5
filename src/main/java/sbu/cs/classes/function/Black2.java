package sbu.cs.classes.function;


import sbu.cs.interfaces.BlackFunction;

public class Black2 implements BlackFunction {

    @Override
    public String func(String arg) {
        StringBuilder ans = new StringBuilder();
        for( int i = 0; i <  arg.length() ; i ++  ){
            ans.append(arg.charAt(i)).append(arg.charAt(i));
        }
        return ans.toString();
    }
}
