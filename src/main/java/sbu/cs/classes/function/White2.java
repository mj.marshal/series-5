package sbu.cs.classes.function;


import sbu.cs.interfaces.WhiteFunction;

public class White2 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        StringBuilder tmp = new StringBuilder(arg2);
        tmp.reverse();
        StringBuilder ans = new StringBuilder(arg1);
        ans.append(tmp);
        return ans.toString();
    }
}
