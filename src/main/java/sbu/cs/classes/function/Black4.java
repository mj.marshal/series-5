package sbu.cs.classes.function;


import sbu.cs.interfaces.BlackFunction;

public class Black4 implements BlackFunction {
    @Override
    public String func(String arg) {
        StringBuilder ans = new StringBuilder();
        ans.append(arg.charAt(arg.length()-1));
        for (int i = 0; i < arg.length() - 1; i++ ){
            ans.append(arg.charAt(i));
        }
        return ans.toString();
    }
}
