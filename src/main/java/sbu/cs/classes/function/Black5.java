package sbu.cs.classes.function;


import sbu.cs.interfaces.BlackFunction;

public class Black5 implements BlackFunction {
    @Override
    public String func(String arg) {
        // a 97 ----> z 122
        StringBuilder ans = new StringBuilder();
        for ( int i = 0; i < arg.length(); i ++ ){
            ans.append( (char) (122 - ( arg.charAt(i) - 'a')) );
        }
        return ans.toString();
    }
}
