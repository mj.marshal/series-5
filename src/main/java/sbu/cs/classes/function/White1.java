package sbu.cs.classes.function;


import sbu.cs.interfaces.WhiteFunction;

public class White1 implements WhiteFunction {
    @Override
    public String func(String arg1, String arg2) {
        String lng;
        String shrt;
        StringBuilder ans = new StringBuilder();
        if( arg1.length() >= arg2.length() ){
            lng = arg1;
            shrt = arg2;
        }
        else {
            lng = arg2;
            shrt = arg1;
        }
        for( int i = 0; i < shrt.length(); i ++ ){
            ans.append(arg1.charAt(i));
            ans.append(arg2.charAt(i));
        }
        for( int i = shrt.length(); i < lng.length(); i ++ ){
            ans.append(lng.charAt(i));
        }
        return ans.toString();
    }
}
