package sbu.cs.classes;


import sbu.cs.data.Direction;
import sbu.cs.interfaces.Square;
import sbu.cs.interfaces.WhiteFunction;

public class PinkSquare implements Square {
    private final WhiteFunction function;
    private Square output;

    private Direction outPutDirection = null;

    private String horizontalInput = null;
    private String verticalInput = null;

    public PinkSquare(WhiteFunction function) {
        this.function = function;
    }


    @Override
    public void setOutputSquare(Square output, Direction direction) {
                outPutDirection = direction;
                this.output = output;

    }

    @Override
    public void setInputString(String input, Direction direction) {
        switch (direction) {
            case VERTICAL:
                this.verticalInput = input;
                break;
            case HORIZONTAL:
                this.horizontalInput = input;
        }
    }

    @Override
    public boolean isReady() {
        return horizontalInput != null && verticalInput != null;
    }

    @Override
    public String calculate() {
        if (!isReady()) return null;

        String result = function.func(horizontalInput, verticalInput);
        output.setInputString(result, outPutDirection);

        return output.calculate();
    }
}
