package sbu.cs.classes;


import sbu.cs.data.Direction;
import sbu.cs.interfaces.BlackFunction;
import sbu.cs.interfaces.Square;

public class BlueSquare implements Square {

    private final BlackFunction function;
    private Square horizontalOutput;
    private Square verticalOutput;
    private String horizontalInput = null;
    private String verticalInput = null;

    public BlueSquare(BlackFunction function){
        this.function = function;
    }

    @Override
    public void setOutputSquare( Square output, Direction direction){
        switch (direction){
            case VERTICAL:
                verticalOutput = output;
                break;
            case HORIZONTAL:
                horizontalOutput = output;
                break;
        }
    }

    @Override
    public void setInputString( String input, Direction direction){
        switch (direction){
            case VERTICAL:
                verticalInput = input;
                break;
            case HORIZONTAL:
                horizontalInput = input;
                break;
        }
    }

    public boolean isReady(){
        return horizontalInput != null || verticalInput != null;
    }

    @Override
    public String calculate(){
        if( !isReady() ) return null;
        if( verticalInput != null ){
            String result = function.func(verticalInput);
            verticalOutput.setInputString(result, Direction.VERTICAL);
            verticalInput = null;
            return verticalOutput.calculate();
        }
        if( horizontalInput != null){
            String result = function.func(horizontalInput);
            horizontalOutput.setInputString(result, Direction.HORIZONTAL);
            horizontalInput = null;
            return horizontalOutput.calculate();
        }
        return null;
    }

}
