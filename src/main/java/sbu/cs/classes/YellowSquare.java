package sbu.cs.classes;


import sbu.cs.data.Direction;
import sbu.cs.interfaces.BlackFunction;
import sbu.cs.interfaces.Square;

public class YellowSquare implements Square {
    private final BlackFunction function;
    private Square output;
    private Direction outPutDirection = null;

    private String input = null;

    public YellowSquare(BlackFunction function) {
        this.function = function;
    }


    @Override
    public void setOutputSquare(Square output, Direction direction) {
                outPutDirection = direction;
                this.output = output;
    }

    @Override
    public void setInputString(String input, Direction direction) {
        this.input = input;
    }

    @Override
    public boolean isReady() {
        return input != null;
    }

    @Override
    public String calculate() {
        if (!isReady()) return null;

        String result = function.func(input);
        output.setInputString(result, outPutDirection);

        return output.calculate();
    }
}
