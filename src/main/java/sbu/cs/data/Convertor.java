package sbu.cs.data;


import sbu.cs.classes.function.*;
import sbu.cs.interfaces.BlackFunction;
import sbu.cs.interfaces.WhiteFunction;

public class Convertor {
    public static BlackFunction blackFunc(int f){
        BlackFunction bf;
        switch (f){
            case 1:
                bf = new Black1();
                break;
            case 2:
                bf = new Black2();
                break;
            case 3:
                bf = new Black3();
                break;
            case 4:
                bf = new Black4();
                break;
            case 5:
                bf = new Black5();
                break;
            default:
                bf = new Black5();
        }
        return bf;
    }

    public static WhiteFunction whiteFunc(int f){
        WhiteFunction wf;
        switch (f){
            case 1:
                wf = new White1();
                break;
            case 2:
                wf = new White2();
                break;
            case 3:
                wf = new White3();
                break;
            case 4:
                wf = new White4();
                break;
            case 5:
                wf = new White5();
                break;
            default:
                wf = new White5();
        }
        return wf;
    }
}
