package sbu.cs;

import sbu.cs.classes.*;
import sbu.cs.data.Convertor;
import sbu.cs.data.Direction;
import sbu.cs.interfaces.Square;

import java.util.ArrayList;

public class App {
    private static Square start;

    /**
     * use this function for magical machine question.
     *
     * @param n     size of machine
     * @param arr   an array in size n * n
     * @param input the input string
     * @return the output string of machine
     */
    public String main(int n, int[][] arr, String input) {
        ArrayList<Square> bottoms = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            bottoms.add(null);
        }

        for( int i = n - 1; i >= 0; i -- ){
            Square right = null;
            for( int j = n - 1; j >= 0; j-- ){
                Square s;
                if (i == 0) {
                    if (j == n -1) {
                        s = new YellowSquare(Convertor.blackFunc(arr[i][j]));
                    } else {
                        s = new GreenSquare(Convertor.blackFunc(arr[i][j]));
                    }
                    if (j == 0) {
                        start = s;
                    }
                } else if (i == n - 1) {
                    if (j == 0) {
                        s = new YellowSquare(Convertor.blackFunc(arr[i][j]));
                    } else {
                        s = new PinkSquare(Convertor.whiteFunc(arr[i][j]));
                        if (j == n - 1) {
                            s.setOutputSquare(new FinalSquare(), Direction.VERTICAL);
                        }
                    }
                } else {
                    if (j == 0) {
                        s = new GreenSquare(Convertor.blackFunc(arr[i][j]));
                    } else if (j == n -1) {
                        s = new PinkSquare(Convertor.whiteFunc(arr[i][j]));
                    } else {
                        s = new BlueSquare(Convertor.blackFunc(arr[i][j]));
                    }
                }
                if (right != null) {
                    s.setOutputSquare(right, Direction.HORIZONTAL);
                }
                if (i < n - 1) {
                    s.setOutputSquare(bottoms.get(j), Direction.VERTICAL);
                }

                right = s;
                bottoms.set(j, s);
            }
        }
        start.setInputString(input, Direction.VERTICAL);
        return start.calculate();
    }

}
