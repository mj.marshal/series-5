package sbu.cs.interfaces;


import sbu.cs.data.Direction;

public interface Square {
    void setOutputSquare(Square neighbour, Direction direction);
    void setInputString(String input, Direction direction);
    boolean isReady();
    String calculate();
}
