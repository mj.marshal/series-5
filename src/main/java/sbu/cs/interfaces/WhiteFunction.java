package sbu.cs.interfaces;

public interface WhiteFunction {
    String func(String arg1, String arg2);
}
