package sbu.cs;

public class SortArray {

    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size) {

        for (int i = 0; i < size - 1; i ++ ){
            int mn = i;
            for( int j = i + 1; j < size; j ++) {
                if (arr[j] < arr[mn]) {
                    mn = j;
                }
            }
            int tmp = arr[mn];
            arr[mn] = arr[i];
            arr[i] = tmp;
        }
        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size) {

        for (int i = 1; i < size; i ++ ){
            int tmp = arr[i];
            int x = i - 1;
            while ( x >= 0 && arr[x] > tmp ) {
                arr[x + 1] = arr[x];
                x --;
            }
            arr[x + 1] = tmp;
        }
        return arr;
    }

    // mergeSort
    private void merge( int[] arr, int l, int m, int r) {

        int sz1 = m - l + 1;
        int sz2 = r - m;
        int[] lft = new int[ sz1 ];
        int[] rt = new int[ sz2 ];
        System.arraycopy(arr, l, lft, 0, sz1);
        for (int i = 0; i < sz2; i ++ ){
            rt[ i ] = arr[ i + m + 1 ];
        }
        int i = 0;
        int j = 0;
        int k = l;
        while( i < sz1 && j < sz2 ){
            if( lft[ i ] <= rt[ j ] ){
                arr[ k ] = lft[ i ];
                i++;
            }
            else {
                arr[ k ] = rt[ j ];
                j++;
            }
            k++;
        }
        while (i < sz1) {
            arr[k] = lft[i];
            i++;
            k++;
        }
        while (j < sz2) {
            arr[k] = rt[j];
            j++;
            k++;
        }
    }

    private void mSort(int[] arr, int lt, int rt) {

        if (lt < rt) {
            int m = lt + ( rt - lt ) / 2;
            mSort( arr, lt, m );
            mSort( arr,m + 1, rt );
            merge( arr, lt, m, rt );
        }

    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size) {
        mSort(arr, 0, size - 1);
        return arr;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value) {
        int l = 0;
        int r = arr.length - 1;

        while( l <= r ){
            int m = l + ( r - l ) / 2;
            if( arr[ m ] == value ){
                return m;
            }
            if( arr[ m ] < value ){
                l = m + 1;
            }
            else {
                r = m - 1;
            }
        }
        return -1;
    }

    private int bSearch(int[] arr, int lt, int rt, int val) {

        if( rt >= lt ){
            int md = lt + ( rt - lt ) / 2;
            if( arr[ md ] == val ){
                return md;
            }
            if( arr[ md ] > val ){
                return bSearch( arr, lt, md - 1, val );
            }
            return bSearch( arr, md + 1, rt, val );
        }
        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value) {
        return bSearch(arr, 0, arr.length - 1, value);
    }
}
